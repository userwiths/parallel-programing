#define _USE_MATH_DEFINES
#include <math.h>   // smallpt, a Path Tracer by Kevin Beason, 2008
#include <stdlib.h> // Make : g++ -O3 smallpt.cpp -o smallpt
#include <stdio.h>  // Usage: time ./smallpt 5000 && xv image.ppm
#include <random>
#include <iostream>

std:: default_random_engine generator;
std::uniform_real_distribution<double> distr(0.0,1.0);
double erand48(short unsigned int* X){
    return distr(generator);
}

struct Vec {
  double x, y, z;                  // position, also color (r,g,b)
  Vec(double x_=0, double y_=0, double z_=0){ x=x_; y=y_; z=z_; }
  Vec operator+(const Vec &b) const { return Vec(x+b.x,y+b.y,z+b.z); }
  Vec operator-(const Vec &b) const { return Vec(x-b.x,y-b.y,z-b.z); }
  Vec operator*(double b) const { return Vec(x*b,y*b,z*b); }
  Vec mult(const Vec &b) const { return Vec(x*b.x,y*b.y,z*b.z); }
  Vec& norm(){ return *this = *this * (1/sqrt(x*x+y*y+z*z)); }
  double dot(const Vec &b) const { return x*b.x+y*b.y+z*b.z; } // cross:
  Vec operator%(Vec&b){return Vec(y*b.z-z*b.y,z*b.x-x*b.z,x*b.y-y*b.x);}
} __attribute__ ((aligned(8)));

struct Ray { Vec o, d;Ray(); Ray(Vec o_, Vec d_) : o(o_), d(d_) {} };
enum Refl_t { DIFF, SPEC, REFR };  // material types, used in radiance()
struct Sphere {
  double rad;       // radius
  Vec p, e, c;      // position, emission, color
  Refl_t refl;      // reflection type (DIFFuse, SPECular, REFRactive)
  Sphere(double rad_, Vec p_, Vec e_, Vec c_, Refl_t refl_):
    rad(rad_), p(p_), e(e_), c(c_), refl(refl_) {}
  double intersect(const Ray &r) const { // returns distance, 0 if nohit
    Vec op = p-r.o; // Solve t^2*d.d + 2*t*(o-p).d + (o-p).(o-p)-R^2 = 0
    double t, eps=1e-4, b=op.dot(r.d), det=b*b-op.dot(op)+rad*rad;
    if (det<0) return 0; else det=sqrt(det);
    return (t=b-det)>eps ? t : ((t=b+det)>eps ? t : 0);
  }
} __attribute__ ((aligned(8)));
struct MultySphere {
  double* rad;       // radius
  Vec *p, *e, *c;      // position, emission, color
  Refl_t *refl;      // reflection type (DIFFuse, SPECular, REFRactive)
  int length;
  double intersect(const Ray &r) const { // returns distance, 0 if nohit
    Vec op = p-r.o; // Solve t^2*d.d + 2*t*(o-p).d + (o-p).(o-p)-R^2 = 0
    double t,t_min,t_max, eps=1e-4;
	double b, det,result=0,swap;
	int i=0;
	for(i=0;i<length;i++){
		b=op.dot(r.d)
		det=b*b-op.dot(op)+rad*rad;
		if (det<0) return 0; else det=sqrt(det);
		t_min=b-det;
		t_max=b+det;
		t=t_min>eps ? t_min : (t_max>eps ? t_max : 0);
		if(result<t){
			result=t;
		}
	}
    return result;
  }
} __attribute__ ((aligned(8)));


//#include "extraScenes.h"
Sphere spheres[] = {//Scene: radius, position, emission, color, material
  Sphere(1e5, Vec( 1e5+1,40.8,81.6), Vec(),Vec(.75,.25,.25),DIFF),//Left
  Sphere(1e5, Vec(-1e5+99,40.8,81.6),Vec(),Vec(.25,.25,.75),DIFF),//Rght
  Sphere(1e5, Vec(50,40.8, 1e5),     Vec(),Vec(.75,.75,.75),DIFF),//Back
  Sphere(1e5, Vec(50,40.8,-1e5+170), Vec(),Vec(),           DIFF),//Frnt
  Sphere(1e5, Vec(50, 1e5, 81.6),    Vec(),Vec(.75,.75,.75),DIFF),//Botm
  Sphere(1e5, Vec(50,-1e5+81.6,81.6),Vec(),Vec(.75,.75,.75),DIFF),//Top
  Sphere(16.5,Vec(27,16.5,47),       Vec(),Vec(1,1,1)*.999, SPEC),//Mirr
  Sphere(16.5,Vec(73,16.5,78),       Vec(),Vec(1,1,1)*.999, REFR),//Glas
  Sphere(600, Vec(50,681.6-.27,81.6),Vec(12,12,12),  Vec(), DIFF) //Lite
};

MultySphere FillMulty(){
	MultySphere multySphere;
	double[9] rad;       // radius
    Vec [9]p, [9]e, [9]c;      // position, emission, color
	Refl_t [9]refl;      // reflection type (DIFFuse, SPECular, REFRactive)
	
	rad[0]=1e5;
	p[0]=Vec( 1e5+1,40.8,81.6);
	e[0]=Vec();
	c[0]=Vec(.75,.25,.25)
	refl[0]=DIFF;
	  
	rad[1]=1e5; p[1]=Vec(-1e5+99,40.8,81.6);e[1]=Vec();c[1]=Vec(.25,.25,.75);refl[1]=DIFF;//Rght
	rad[2]=1e5; p[2]=Vec(50,40.8, 1e5);     e[2]=Vec();c[2]=Vec(.75,.75,.75);refl[2]=DIFF;//Back
	rad[3]=1e5; p[3]=Vec(50,40.8,-1e5+170); e[3]=Vec();c[3]=Vec();           refl[3]=DIFF;//Frnt
	rad[4]=1e5; p[4]=Vec(50, 1e5, 81.6);    e[4]=Vec();c[4]=Vec(.75,.75,.75);refl[4]=DIFF;//Botm
	rad[5]=1e5; p[5]=Vec(50,-1e5+81.6,81.6);e[5]=Vec();c[5]=Vec(.75,.75,.75);refl[5]=DIFF;//Top
	rad[6]=16.5;p[6]=Vec(27,16.5,47);       e[6]=Vec();c[6]=Vec(1,1,1)*.999;refl[6]=SPEC;//Mirr
	rad[7]=16.5;p[7]=Vec(73,16.5,78);       e[7]=Vec();c[7]=Vec(1,1,1)*.999;refl[7]=REFR;//Glas
	rad[8]=600; p[8]=Vec(50,681.6-.27,81.6);e[8]=Vec(12,12,12);c[8]=Vec();refl[8]=DIFF; //Lite
	
	multySphere.rad=rad;
	multySphere.p=p;
	multySphere.e=e;
	multySphere.c=c;
	multySphere.refl=refl;
	multySphere.length=9;
	return multySphere;
}

inline double clamp(double x){ 
	return x < 0 ? 0 : x > 1 ? 1 : x; 
}
inline int toInt(double x){
	return int(pow(clamp(x),1/2.2)*255+.5);
}
inline bool intersect(const Ray &r, double &t, int &id){
  double n=sizeof(spheres)/sizeof(Sphere), d, inf=t=1e20;
  bool condition;
  int i=0,n_condition=(int)n;
  //#pragma omp parallel for
  for(i=0;i<n_condition;i++){
	condition=(d=spheres[i].intersect(r))&&d<t;
	t=condition?d:t;
	id=condition?i:id;
  }
  return t<inf;
}

Vec radiance(const Ray &r, int depth, unsigned short *Xi,int e=1){
  double t;  // distance to intersection  
  double r1,r2,r2s,p,a,b,c;
  double nt,nc,nnt,ddn,cos2t,omega;
  double Re,TP,RP,Tr,R0,P,cos_a_max,cos_a,phi,eps1,eps2,sin_a;
  int i,id=0,number_of_spheres=sizeof(spheres)/sizeof(Sphere);                               // id of intersected object
  bool into,depth_condition,obj_ref_diff_condition,obj_ref_spec_condition,return_condition=false;
  Vec w,d,u,v,x,n,nl,l,f,e_vector,tdir,sv,result_vector,sw,su;
  if (!intersect(r, t, id)) return Vec(); // if miss, return black
  
  const Sphere &obj = spheres[id];        // the hit object
  x=r.o+r.d*t;
  n=(x-obj.p).norm();
  nl=n.dot(r.d)<0?n:n*-1;
  f=obj.c;
  p = f.x>f.y && f.x>f.z ? f.x : f.y>f.z ? f.y : f.z; // max refl
  
  result_vector=obj.e;
  depth+=1;
  depth_condition=depth>5;
  f=erand48(Xi)&&depth_condition<p?f*(1/p):f;
  return_condition=depth_condition || (f.x==obj.c.x && f.y==obj.c.y &&f.z==obj.c.z);
  if(return_condition){
	return obj.e*e;
  }
  //R.R.  
  
  obj_ref_diff_condition=obj.refl == DIFF;
  obj_ref_spec_condition=obj.refl == SPEC;
  // Ideal DIFFUSE reflection
  r1=2*M_PI*erand48(Xi);
  r2=erand48(Xi);
  r2s=sqrt(r2);
  w=nl, u=((fabs(w.x)>.1?Vec(0,1):Vec(1))%w).norm(), v=w%u;
  d = (u*cos(r1)*r2s + v*sin(r1)*r2s + w*sqrt(1-r2)).norm();
  
  return_condition=!return_condition && (obj_ref_diff_condition || obj_ref_spec_condition);
  if(obj_ref_diff_condition){
	  #pragma omp parallel for
	  for (i=0; i<number_of_spheres; i++){
		  const Sphere &s = spheres[i];
		  if (s.e.x<=0 && s.e.y<=0 && s.e.z<=0) continue; // skip non-lights
		  
		  sw=s.p-x;
		  su=((fabs(sw.x)>.1?Vec(0,1):Vec(1))%sw).norm();
		  sv=sw%su;
		  cos_a_max = sqrt(1-s.rad*s.rad/(x-s.p).dot(x-s.p));
		  eps1 = erand48(Xi);
		  eps2 = erand48(Xi);
		  cos_a = 1-eps1+eps1*cos_a_max;
		  sin_a = sqrt(1-cos_a*cos_a);
		  phi = 2*M_PI*eps2;
		  l = su*cos(phi)*sin_a + sv*sin(phi)*sin_a + sw*cos_a;
		  l.norm();
		  if (intersect(Ray(x,l), t, id) && id==i){  // shadow ray
			omega = 2*M_PI*(1-cos_a_max);
			e_vector = e_vector + f.mult(s.e*l.dot(nl)*omega)*M_1_PI;  // 1/pi for brdf
		  }
	}  
	return obj.e*e+e_vector+f.mult(radiance(Ray(x,d),depth,Xi,0));
  }
  if(obj_ref_spec_condition){
	  return obj.e + f.mult(radiance(Ray(x,r.d-n*2*n.dot(r.d)),depth,Xi));
  }
  
  Ray reflRay(x, r.d-n*2*n.dot(r.d));     // Ideal dielectric REFRACTION
  into = n.dot(nl)>0;                // Ray from outside going in?
  nc=1;
  nt=1.5;
  nnt=into?nc/nt:nt/nc;
  ddn=r.d.dot(nl);
  
  return_condition=!return_condition && 1-nnt*nnt*(1-ddn*ddn)<0;
  // Total internal reflection
  if(return_condition){
	  return obj.e + f.mult(radiance(reflRay,depth,Xi));
  }
  
  tdir = (r.d*nnt - n*((into?1:-1)*(ddn*nnt+sqrt(cos2t)))).norm();
  a=nt-nc;
  b=nt+nc;
  R0=a*a/(b*b);
  c = 1-(into?-ddn:tdir.dot(n));
  Re=R0+(1-R0)*c*c*c*c*c;
  Tr=1-Re;
  P=.25+.5*Re;
  RP=Re/P;
  TP=Tr/(1-P);
  
  return obj.e + f.mult(depth>2 ? (erand48(Xi)<P ?   // Russian roulette
    radiance(reflRay,depth,Xi)*RP:radiance(Ray(x,tdir),depth,Xi)*TP) :
    radiance(reflRay,depth,Xi)*Re+radiance(Ray(x,tdir),depth,Xi)*Tr);
}

int main(int argc, char *argv[]){
  int i=0,w=1024, h=768;
  int samps = argc==2 ? atoi(argv[1])/4 : 1; // # samples
  int x,y,s;
  double dx,dy;
  unsigned short Xi[3]={0,0,0};
  double r1,r2;
  Ray cam(Vec(50,52,295.6), Vec(0,-0.042612,-1).norm()); // cam pos, dir
  Vec cx=Vec(w*.5135/h), cy=(cx%cam.d).norm()*.5135, r,d, *c=new Vec[w*h];
  
  
  #pragma omp target teams
  {
   #pragma omp parallel for collapse(5)       // OpenMP
   for (y=0; y<h; y++){                       // Loop over image rows
	for ( x=0; x<w; x++){   // Loop cols
	  for (int sy=0; sy<2; sy++)     // 2x2 subpixel rows
		for (int sx=0; sx<2; sx++){        // 2x2 subpixel cols
		  for (int s=0; s<samps; s++){
			//fprintf(stderr,"\rRendering (%d spp) %5.2f%%",samps*4,100.*y/(h-1));
			r=Vec();
			Xi[2]=y*y*y;
				
			double r1=2*erand48(Xi), dx=r1<1 ? sqrt(r1)-1: 1-sqrt(2-r1);
			double r2=2*erand48(Xi), dy=r2<1 ? sqrt(r2)-1: 1-sqrt(2-r2);
			Vec d = cx*( ( (sx+.5 + dx)/2 + x)/w - .5) +
					cy*( ( (sy+.5 + dy)/2 + y)/h - .5) + cam.d;
			r = r + radiance(Ray(cam.o+d*140,d.norm()),0,Xi,5)*(1./samps);
				
			if(s==samps-1){
				i=(h-y-1)*w+x;
				c[i] = c[i] + Vec(clamp(r.x),clamp(r.y),clamp(r.z))*.25;
			}
		  } // Camera rays are pushed ^^^^^ forward to start in interior
		}
	  }
	}
  }
  printf("q");
  FILE *f = fopen("image.ppm", "w");         // Write image to PPM file.
  fprintf(f, "P3\n%d %d\n%d\n", w, h, 255);
  #pragma omp for private(i)
  for (i=0; i<w*h; i++)
    fprintf(f,"%d %d %d ", toInt(c[i].x), toInt(c[i].y), toInt(c[i].z));
}
